#include <util/twi.h>
#include <avr/interrupt.h>
#include "i2c-slave.h"

#define BUFLEN (1 << 5)
uint8_t received[BUFLEN];


#define I2COK (_BV(TWEN)| _BV(TWIE))
#define I2CACK (I2COK | _BV(TWINT) | _BV(TWEA)) 
#define I2CNACK (I2COK | _BV(TWINT))
#define I2CENABLE (_BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT))

typedef void (*ReceiveCallback)(int length, uint8_t buffer[]);
typedef void (*WriteCallback)(int length);

ReceiveCallback receive_cb;
WriteCallback write_cb;

void i2c_init(uint8_t address, ReceiveCallback rcb, WriteCallback wcb) {
    receive_cb = rcb;
    write_cb = wcb;
    TWAR = (unsigned char) (address << 1 | 1);
    TWCR = I2CENABLE;    
}

/* Debug */
volatile int out_status;

/* Position in the current written command. Private to ISR */
uint8_t position;
/* Should point to the data to send */
volatile uint8_t *out_buffer; 
/* Should be the size of the data to send */
volatile uint8_t out_buffer_size;
extern volatile int out_status;

void set_out(uint8_t *buffer, uint8_t size) {
  out_buffer = buffer;
  out_buffer_size = size;
}

ISR(TWI_vect)
{
  int status = TW_STATUS;
  switch(status)
  {
    case TW_SR_SLA_ACK:
    case TW_SR_GCALL_ACK:
      position = 0;
      TWCR = I2CACK; 
      break;
    case TW_SR_DATA_ACK:
    case TW_SR_GCALL_DATA_ACK:
      // received data from master, call the receive callback
      received[position++] = TWDR;
      position++;
      TWCR = (position < BUFLEN) ? I2CACK : I2CNACK; 
      break;
    case TW_SR_DATA_NACK:
    case TW_SR_GCALL_DATA_NACK:
      // received data from master but we NACK : buffer full.
      // Already sent a NACK. TWEA off would disconnect.
      TWCR = (1<<TWSTO)|(1<<TWINT);
      break;
    case TW_SR_STOP:
      // Old school and happy to be.
      (*receive_cb)(position, received);
      TWCR = I2CACK;
      break;
    case TW_ST_SLA_ACK:
      // master is requesting data, call the request callback
      position = 0;
      TWDR = out_buffer[position++];
      TWCR = (position < out_buffer_size) ? I2CACK : I2CNACK;
      break;
    case TW_ST_DATA_ACK:
      // master is requesting data, call the request callback
      TWDR = out_buffer[position++];
      TWCR = (position < out_buffer_size) ? I2CACK : I2CNACK;
      break;
    case TW_ST_DATA_NACK:
       // We were nack. Stop transmission
       TWCR = I2CACK;
       (*write_cb)(position);
       break;
    case TW_ST_LAST_DATA:
      // We did a nack last time. No more data to send.
      TWCR = I2CACK;
      (*write_cb)(position);
      break;
    default: // eg BUS_ERROR
      TWCR = I2CACK;
      break;
  }
} 
