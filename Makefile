CC=avr-gcc
ADDRESS=48
LOCATION=0
CFLAGS=-Os -DF_CPU=16000000UL -mmcu=atmega328p


TARGET=dccjs-occupancy-$(ADDRESS)

$(TARGET).hex: i2c-slave.o $(TARGET).o
	avr-gcc -mmcu=atmega328p $^ -o $(TARGET).elf
	avr-objcopy -O ihex -R .eeprom $(TARGET).elf $(TARGET).hex

$(TARGET).o: dcc_occupancy.c
	$(CC) $(CFLAGS) -DSLAVE_ADDRESS=$(ADDRESS) -DLOCATION=$(LOCATION) -c $< -o $@

compile: $(TARGET).hex

install:
	avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyUSB0 -b 57600 -U flash:w:$(TARGET).hex

clean:
	-rm *.o *.elf *.hex
