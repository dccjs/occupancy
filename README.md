# DCCJS-occupancy
A program to control a set of occupancy sensors and give back the result to a controlling system
over the I2C bus.

Compared to an MCP23017 gpio extensions, this program adds some preliminary software based debouncing.

The code will execute on an Arduino nano or uno. It can provide 16 inputs if serial TX/RX are disabled.
A3 is used for interupt when an input is modified.

