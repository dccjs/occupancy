#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include "i2c-slave.h"

/* Constant: I2C slave address of the arduino */
#ifndef SLAVE_ADDRESS
#error "SLAVE_ADDRESS must be defined"
#endif

/* How many time must we read a value before we consider it as right. */
#define LIMIT_DEBOUNCE 4
/* Time between each read - resolution at best 0.1 ms */
#define DELAY 2.0
/* Interupt - We use PIN 13 */
#define PIN_INTERUPT 5
#define PORT_INTERUPT PORTB
#define DDR_INTERUPT DDRB
/* Masks */
#define MASK_D 0xFC
#define MASK_B 0x1F
#define MASK_C 0xF

/* The official current state of the inputs. */
volatile uint16_t current = 0;
/* The new set of raising pins */
volatile uint16_t raising = 0;

/* What was just read previously but not yet official because of debouncing */
uint16_t candidate = 0;
/* Debouncing counter */
uint16_t counter = 0;
/* Mask used to select pins kept in the result */
uint16_t mask = 0x3FFF;
/* To avoid double activation of same sensor */
uint16_t previous_mask = 0xFFFF;
/* To avoid double activation of same sensor */
uint16_t prev_prev_mask = 0xFFFF;
uint16_t clock = 0;

void onWrite(int len) {
#if LOCATION==1
  previous_mask = ~raising;
  raising = 0;
#endif
  // Reset signal to RPi
  PORT_INTERUPT |= _BV(PIN_INTERUPT);
}

void onRead(int len, uint8_t buf[]) {
  if (len <= 0) return;
  switch (buf[0] & 0xF0) {
    case 0x8:
      PORT_INTERUPT |= _BV(PIN_INTERUPT);
    case 0x4:
      PORT_INTERUPT &= ~_BV(PIN_INTERUPT);
    case 0x10:
      if (len != 3) return;
      mask = (short)(buf[1]) << 8 | buf[2];
      break;
  }
}

void gpio_init() {
  DDRD &= ~MASK_D;
  PORTD = MASK_D;

  DDRB &= ~MASK_B;
  PORTB |= MASK_B;

  DDRC &= ~MASK_C;
  PORTC |= MASK_C;

  DDR_INTERUPT |= _BV(PIN_INTERUPT);
  PORT_INTERUPT |= _BV(PIN_INTERUPT);
}

void init() {
  gpio_init();
  // Serial debugging
  cli();
#if LOCATION==1
  set_out((uint8_t *)&raising, 2);
#else
  set_out((uint8_t *)&current, 2);
#endif
  i2c_init(SLAVE_ADDRESS, onRead, onWrite);
  sei();
}


void loop() {
  // candidate computation from pins value through registers.
  // invert it to get 1 for presence and 0 for absence.
  uint16_t next = 0;
#if LOCATION==1
  uint16_t change;
#endif
  for (int i = 0; i < 3; i++) {
    _delay_us(10.0);
    next |= ~(((PIND & MASK_D) >> 2) | ((PINB & MASK_B) << 6) |
              ((PINC & MASK_C) << 11)) &
            mask;
  }
  if (next != current) {
    if (next == candidate) {
      if (++counter > LIMIT_DEBOUNCE) {
#if LOCATION==1
        // Keep only raising edge transitions.
        change = (next ^ current) & next;
        // Must have not been recently triggered
        change = change & previous_mask & prev_prev_mask;
        if(change) {
          raising |= change;
          PORT_INTERUPT &= ~_BV(PIN_INTERUPT);
        }
#else
        // Set signal to RPi
        PORT_INTERUPT &= ~_BV(PIN_INTERUPT);
#endif
        current = next;
        counter = 0;
      }
    } else {
      counter = 0;
    }
  }
  candidate = next;
#if LOCATION==1
  clock = (clock + 1) & 3;
  if (clock == 0) {
    prev_prev_mask = previous_mask;
    previous_mask = 0xFFFF;
  }
#endif
  _delay_ms(DELAY);
}

void main() {
  init();
  while (1) loop();
}
